import React, { Component } from 'react';
import api from '../../../services/api';
import webserver from '../../../services/webserver';
import Header from '../../../components/header';
import Link from 'next/link';
import Analytics from '../../../components/analytics';

const Post = ({ post, content }) => (
   <>
        <div className="wrapper">
            <Header />
            <style global jsx>{`
                    html, body, div, span, applet, object, iframe,
                    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
                    a, abbr, acronym, address, big, cite, code,
                    del, dfn, em, img, ins, kbd, q, s, samp,
                    small, strike, strong, sub, sup, tt, var,
                    b, u, i, center,
                    dl, dt, dd, ol, ul, li,
                    fieldset, form, label, legend,
                    table, caption, tbody, tfoot, thead, tr, th, td,
                    article, aside, canvas, details, embed, 
                    figure, figcaption, footer, header, hgroup, 
                    menu, nav, output, ruby, section, summary,
                    time, mark, audio, video {
                        margin: 0;
                        padding: 0;
                        border: 0;
                        font-size: 100%;
                        font: inherit;
                        vertical-align: baseline;
                    }
                    /* HTML5 display-role reset for older browsers */
                    article, aside, details, figcaption, figure, 
                    footer, header, hgroup, menu, nav, section {
                        display: block;
                    }
                    body {
                        line-height: 1;
                        background-color: #fefefe;
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 16px;
                        line-height: 1.5;
                        color: #555;
                    }
                    ol, ul {
                        list-style: none;
                    }
                    blockquote, q {
                        quotes: none;
                    }
                    blockquote:before, blockquote:after,
                    q:before, q:after {
                        content: '';
                        content: none;
                    }
                    table {
                        border-collapse: collapse;
                        border-spacing: 0;
                    }

                    .wrapper {
                        display: flex;
                        flex-direction: row;
                        box-sizing: border-box;
                    }

                    .back-button {
                        width: 100%;
                        height: 32px;
                        text-align: center;
                    }

                    .back-button a {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 22px;
                        text-decoration: none;
                        color: #00848C;
                    }

                    h1{
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 22px;
                    }

                    .images {
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }

                    .content p {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 16px;
                        line-height: 1.5;
                        color: #555;
                        text-indent: 1.5em;
                    }

                    .content h1 {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 22px;
                    }

                    .content ul {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 16px;
                        line-height: 1.5;
                        color: #555;
                        text-indent: 1.5em;
                        list-style: inside;
                    }

                    .post {
                        background-color: #fefefe;
                        box-sizing: border-box;
                        margin-top: 50px;
                        width: 75%;

                        display: flex;
                        flex-direction: column;
                        justify-content: flex-start;
                        align-items: center;
                    }

                    .post article{
                        width: 90%;
                        margin-top: 20px;
                        margin-bottom: 50px;
                    }

                    .post article img{
                        max-width: 100%;
                        max-height: 300px;
                        width: auto;
                        height: auto;

                    }

                    @media (max-width: 700px) {

                        .wrapper {
                            flex-direction: column;
                        }

                        h1 {
                            text-align: center;
                        }

                        .post {
                            margin-top: 50px;
                            width: 100%;
                        }

                        img {
                            max-width: 100%;
                            max-height: 100%;
                            width: auto;
                            height: auto;
                        }
                    }

                    @media (min-width: 701px) and (max-width: 1200px){

                        .post {
                            margin-top: 50px;
                        }

                        img {
                            max-width: 100%;
                            max-height: 100%;
                            width: auto;
                            height: auto;
                        }

                    }

            `}</style>

            <div className="post">
                <div className="back-button">
                    <Link href="/">
                        <a>« voltar</a>
                    </Link>
                </div>
                <article>
                    <h1>{post.subject}</h1>< br/>
                    <div className="content" dangerouslySetInnerHTML={{__html:content}} ></div>
                </article>
                            
            </div>

        <Analytics />
        </div>
    </>
);

Post.getInitialProps = async ({ query }) => {
    const response = await api.get(`/post/${query.slug}`);
    const content = await webserver.get(`/html/${response.data.filename}`);

    return { post: response.data, content: content.data};
};

export default Post;