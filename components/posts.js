import React, { Component } from 'react';
import api from '../services/api';
import Link from 'next/link';


export default class Posts extends Component {

    state = {
        posts: [],
    }

    componentDidMount(){
        this.loadPosts();
    }

    loadPosts = async () => {
        const response = await api.get('/post');

        let filteredResponse = [];

        response.data.map((post) => {
            if(post.status == 'enabled') {
                filteredResponse.push(post);
            }
        });

        this.setState({ posts: filteredResponse})
    }

    render (){

        const {posts} = this.state;

        return (
            <div className="wrapper">
                <style jsx>{`

                    .wrapper {
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-start;
                        align-items: center;
                        width: 75%;
                        box-sizing: border-box;
                    }

                    h1{
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 22px;
                    }

                    h1:hover{
                        color: #004156;
                        text-decoration: underline #004156;
                    }

                    p {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 16px;
                        line-height: 1.5;
                        color: #555;
                        text-indent: 1.5em;
                    }

                    .post-list {
                        background-color: #fefefe;
                        box-sizing: border-box;
                        margin-top: 50px;

                        display: flex;
                        flex-direction: column-reverse;
                        justify-content: center;
                        align-items: center;
                    }

                    .post-list article{
                        width: 90%;
                        margin: 20px 0px;
                    }

                    .post-list a {
                        text-decoration: none;
                        color: #333;
                        transaction: all 0.2s;
                    }

                    span {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 12px;
                        line-height: 2;
                        color: #555; 
                        padding: 3px;
                        box-sizing: border-box;
                    }

                    .date {
                        color: #004156
                    }

                    .tags {
                        background-color: #11959D;
                        color: #ffffff;
                        border-radius: 3px;
                        font-weight: bold;
                        margin-right: 10px;
                    }

                    a#read-more {
                        font-family: Arial, "Helvetica Neue", Helvetica;
                        font-size: 12px;
                        line-height: 2;
                        color: blue; 
                        padding: 3px;
                        box-sizing: border-box;
                    }

                    @media (max-width: 700px){
                        .wrapper {
                            width: 100%;
                        }

                        h1 {
                            text-align: center;
                        }

                        .post-list {
                            margin-top: 50px;
                        }
            
                    }

                    @media (min-width: 701px) and (max-width: 1200px){

                        .post-list {
                            margin-top: 50px;
                        }
            
                    }

                `}</style>

                <div className="post-list">
                    {posts.map(post => (
                        <article key={post.id}>
                            <Link href="/post/[post]" as={`/post/${post.slug}`}>
                                <a>
                                    <h1>{post.subject} </h1><br />
                                </a>
                            </Link>
                            
                            <p>{post.shortDescription}  
                                    <Link href="/post/[post]" as={`/post/${post.slug}`}>
                                        <a id="read-more">
                                            ...leia mais<br />
                                        </a>
                                    </Link>              
                            </p><br/>
                            
                            <span className="date">Criado em {post.createDate} ~ Atualizado em {post.lastModified} </span><br />
                            
                            {post.tag.split(",").map(key => (
                                <span className="tags">{key}</span>
                            ))}

                        </article>
                    ))}
                </div>
             </div>
        )
    }
}
