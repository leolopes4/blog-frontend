import React from 'react';

const Header = () => (
    <header className="wrapper header"> 
        <style jsx>{`
            .wrapper{
                width: 25%;
                min-height: 100%;
                height: auto;
                display: flex;
                flex-direction: column;
                justify-content: flex-start;
                align-content: center;
                align-items: center;
                box-sizing: border-box;
            }

            .about-me{
                height: 100vh;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-content: center;
                align-items: center;
                line-height: 1;
            }

            h1 {
                font-family: Arial, "Helvetica Neue", Helvetica;
                font-size: 22px;
                font-weight: bold;
                text-align: center;
                color: #fff;
            }

            p {
                font-family: Arial, "Helvetica Neue", Helvetica;
                font-size: 16px;
                color: #fff;
                text-align: center;
                line-height: 1.5;
            }

            .header {
                background-color: #00848C ;
            }

            .profile {
                grid-area: profile;
                width: 100%;
                height: 210px;

                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;

                box-sizing: border-box;

            }

            .profile>picture>img{
                width: 150px;
                height: 150px;
                border-radius: 100px;
                -webkit-transform: scaleX(-1);
            }

            .profile>h1 {
                margin-top: 20px;
            }

            .career {
                width: 100%;
                padding: 100px 0px;

                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }

            .career>p{
                text-align: center;
            }

            .social-media {
                grid-area: social;
                width: 100%;

                display: flex;
                justify-content: space-around;
                align-items: center;
            }
            
            .social-media img {
                width: 32px;
                height: 32px;
                opacity: 0.6;
            }

            .social-media img:hover {
                opacity: 1.0;
            }

            @media (max-width: 700px){
                .wrapper {
                    width: 100%;
                    height: 300px;
                }

                .career {
                    display: none;
                }

                .social-media {
                    width: 50%;
                    margin-top: 20px;
                    align-items: center;
                }

                .social-media img {
                    width: 22px;
                    height: 22px;
                    opacity: 0.6;
                }

            }

            @media (min-width: 701px) and (max-width: 1200px){
                .career {
                    display: none;
                }

                p {
                    display: none;
                }

                .social-media {
                    width: 50%;
                    margin-top: 20px;
                    align-items: center;
                }

            }


        `}</style>

        <div className="about-me">

            <div className="profile">

                <picture>
                    <img src="https://www.leonardo-lopes.com/images/profile.jpeg" alt="" />
                </picture>
                
                <h1> Leonardo Lopes </h1>
                <p>Engenheiro de Software Senior na GFT Brasil</p>
            
            </div>

            <div className="career">
                <p> Graduado em ciências da computação<br /> Cursando pós graduação na PUC-MINAS <br /> Gamer e entusiasta por tecnologia</p>
            </div>

            <div className="social-media">
                <picture className="linkedin">
                    <a href="https://www.linkedin.com/in/leonardo-lopes-nascimento-210545111/" target="_blank">
                        <img src="https://www.leonardo-lopes.com/images/linkedin.png" alt="" />
                    </a>
                </picture>

                <picture className="twitter">
                    <a href="https://twitter.com/Leo_lpees" target="_blank">
                        <img src="https://www.leonardo-lopes.com/images/twitter.png" alt="" />
                    </a>
                </picture>
            </div>
        </div>            
     </header>
);

export default Header;