FROM node:latest

RUN git clone https://gitlab.com/leolopes4/blog-frontend.git ;\
    git checkout feat/analytics;\
    cd blog-frontend ;\
    npm install;\
    yarn run build

WORKDIR /blog-frontend/

ENTRYPOINT ["yarn", "run", "start"]   
 
EXPOSE 3000
