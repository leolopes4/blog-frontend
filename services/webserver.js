import axios from 'axios';

const webserver = axios.create({
    baseURL: 'https://www.leonardo-lopes.com'
})

export default webserver;