import axios from 'axios';
import https from 'https';

const URL = process.env.URL_ORIGEM;
const PORT = process.env.PORT;

const api = axios.create({
    baseURL: `${URL}:${PORT}/api/v1`,
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
})

export default api;